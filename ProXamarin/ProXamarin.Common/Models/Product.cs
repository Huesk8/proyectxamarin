﻿namespace ProXamarin.Common.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Product
    {
        public Product()
        {
            ProductKey = Guid.NewGuid();
            PublisnOn = DateTime.Now;
        }

        [Key]
        public int ProductId { get; set; }
        public Guid ProductKey { get; set; }
        [Required]
        [StringLength(50)]
        public string Description{ get; set; }

        [DisplayFormat(DataFormatString = "{0:C2}", ApplyFormatInEditMode = false)]
        public Decimal Price { get; set; }
        [Display(Name = "Activo")]
        public bool IsAviable { get; set; }
        [Display(Name = "Publish On")]
        public DateTime PublisnOn { get; set; }
        [Display (Name ="Imagen")]
        public string ImagePath { get; set; }
        [DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        public string  ImageFullPath
        {
            get
            {
                if (string.IsNullOrEmpty(this.ImagePath))
                    return "Noproduct";

                if (this.ImagePath.Length >55)
                    return $"http://54.152.5.165:40004/apiservice/{this.ImagePath.Substring(1)}";
                else
                    return $"http://54.152.5.165:40004/{this.ImagePath.Substring(1)}";
            }
        }

        public override string ToString()
        {
            return this.Description;
        }

        [NotMapped]
        public byte[] ImageArray { get; set; }
    }
}
