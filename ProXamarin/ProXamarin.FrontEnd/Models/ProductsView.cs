﻿
namespace ProXamarin.FrontEnd.Models
{
    using System.Web;
    using Common.Models;
     
    public class ProductsView : Product
    {
        public HttpPostedFileBase ImageFile { get; set; }
    }
}