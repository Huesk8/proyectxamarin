﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ProXamarin.FrontEnd.Startup))]
namespace ProXamarin.FrontEnd
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
