﻿

namespace ProXamarin.Domain.Models
{
    using System.Data.Entity;
    using ProXamarin.Common.Models;

    public class DataContext: DbContext
    {
        public DataContext(): base("DefaultConnection")
        {

        }

        public DbSet<Product> Products { get; set; }
    }
}
